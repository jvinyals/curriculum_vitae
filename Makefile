
all: view

view: curriculum.pdf
	okular $<

curriculum.pdf: curriculum.tex
	pdflatex $^

clean:
	rm -f *.aux
	rm -f *.dvi
	rm -f *.fdb_latexmk
	rm -f *.fls
	rm -f *.log

clean_all: clean
	rm -f curriculum.pdf

